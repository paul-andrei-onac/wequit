/*!
 * WeQuit (https://www.npmjs.com/package/wequit)
 * Copyright 2023 Paul Andrei Onac
 * Licensed under MIT (https://gitlab.com/paul-andrei-onac/wequit/blob/master/LICENSE)
 */

document.addEventListener('DOMContentLoaded', function() {
  // Create the modal container element
  var modalContainer = document.createElement('div');
  modalContainer.classList.add('wequit-container');

  // Create the modal content element
  var modalContent = document.createElement('div');
  modalContent.classList.add('wequit-body');

  // Create the additional content div
  var additionalContent = document.createElement('div');
  additionalContent.innerHTML = 'I QUIT !';

  // Append modal content and additional content to the container
  modalContainer.appendChild(modalContent);
  modalContent.appendChild(additionalContent);

  // Add the modal container to the document body
  document.body.appendChild(modalContainer);

  // Show the modal
  modalContainer.style.display = 'block';
});
